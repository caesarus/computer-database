package cdb;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

@Configuration
public class AppConfig
{
    @Value("${paging.page}")
    private int defaultPage;

    @Value("${paging.size}")
    private int defaultSize;

    @Bean
    public Pageable defaultPaging() {
        return PageRequest.of(defaultPage, defaultSize);
    }
}
