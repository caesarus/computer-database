package cdb.controller;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.Optional;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RequestMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
public abstract class BaseController
{
    protected static final String ID = "/{id}";

    @Resource
    private Pageable defaultPaging;

    protected Pageable paging(Optional<Integer> page, Optional<Integer> size) {
        return (page.isPresent() && size.isPresent())
            ? PageRequest.of(page.get(), size.get())
            : defaultPaging;
    }
}
