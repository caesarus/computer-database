package cdb.controller;

import cdb.model.Company;
import cdb.service.core.CompanyService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.Optional;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;

@RestController
@RequestMapping("companies")
public class CompanyController extends BaseController
{
    @Resource
    private CompanyService service;

    @GetMapping
    public Page<Company> pageAll(@RequestParam Optional<Integer> page, @RequestParam Optional<Integer> size) {
        Pageable paging = this.paging(page, size);
        return service.pageAll(paging);
    }

    @PostMapping
    @ResponseBody
    @ResponseStatus(CREATED)
    public Company createOne(@RequestBody @Valid Company company) {
        return service.create(company);
    }

    @PutMapping(ID)
    @ResponseBody
    public Company updateOne(@PathVariable long id, @RequestBody @Valid Company company) {
        return service.update(id, company);
    }

    @DeleteMapping(ID)
    @ResponseStatus(NO_CONTENT)
    public void deleteOne(@PathVariable long id) {
        service.delete(id);
    }
}
