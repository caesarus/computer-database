package cdb.controller;

import cdb.model.Computer;
import cdb.service.core.ComputerService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.Optional;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;

@RestController
@RequestMapping("computers")
public class ComputerController extends BaseController
{
    @Resource
    private ComputerService service;

    @GetMapping
    public Page<Computer> pageAll(@RequestParam Optional<Integer> page, @RequestParam Optional<Integer> size) {
        Pageable paging = this.paging(page, size);
        return service.pageAll(paging);
    }

    @PostMapping
    @ResponseBody
    @ResponseStatus(CREATED)
    public Computer createOne(@RequestBody @Valid Computer computer) {
        return service.create(computer);
    }

    @PutMapping(ID)
    @ResponseBody
    public Computer updateOne(@PathVariable long id, @RequestBody @Valid Computer computer) {
        return service.update(id, computer);
    }

    @DeleteMapping(ID)
    @ResponseStatus(NO_CONTENT)
    public void deleteOne(@PathVariable long id) {
        service.delete(id);
    }
}
