package cdb.controller;

import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.persistence.EntityNotFoundException;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@EnableWebMvc
@ControllerAdvice
public class RestExceptionHandler
{
    @Bean
    public DispatcherServlet dispatcherServlet () {
        DispatcherServlet ds = new DispatcherServlet();
        //ds.setThrowExceptionIfNoHandlerFound(true); // neutralized code
        return ds;
    }

    @ExceptionHandler
    @ResponseStatus(NOT_FOUND)
    @ResponseBody
    public String handleNoHandlerFound(NoHandlerFoundException ex) {
        // if you want to customize a different error message :)
        return ex.getMessage();
    }

    @ExceptionHandler
    @ResponseStatus(NOT_FOUND)
    @ResponseBody
    public String handleEntityNotFound(EntityNotFoundException ex) {
        return ex.getMessage();
    }
}
