package cdb.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class Company implements Serializable
{
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @NotNull
    @NotBlank
    @Size(max = 255)
    @Column(nullable = false)
    private String name;

    @JsonIgnore
    @OneToMany(mappedBy = "company", cascade = ALL)
    private List<Computer> computers;

    // utils

    public Company apply(Company company) {
        if (company != null) {
            this.id = company.id;
            this.name = company.name;
            this.computers = company.computers;
        }
        return this;
    }

    @Builder
    private Company(Long id, String name, List<Computer> computers) {
        this.id = id;
        this.name = name;
        this.computers = computers;
    }
}
