package cdb.model;

import cdb.valid.constraint.FutureDiscontinued;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.Optional;

import static javax.persistence.GenerationType.IDENTITY;
import static javax.persistence.TemporalType.TIMESTAMP;

@Getter
@Setter
@NoArgsConstructor
@Entity
@FutureDiscontinued
public class Computer implements Serializable
{
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @NotNull
    @NotBlank
    @Size(max = 255)
    @Column(nullable = false)
    private String name;

    @Column
    @Temporal(TIMESTAMP)
    private Date introduced;

    @Column
    @Temporal(TIMESTAMP)
    private Date discontinued;

    @JsonIgnore
    @ManyToOne
    @JoinColumn
    private Company company;

    // utils

    public Computer apply(Computer computer) {
        if (computer != null) {
            this.name = computer.name;
            this.introduced = computer.introduced;
            this.discontinued = computer.discontinued;
            this.company = computer.company;
        }
        return this;
    }

    @JsonProperty("companyId")
    private void setCompanyId(Long companyId) {
        this.company = new Company();
        this.company.setId(companyId);
    }

    @JsonProperty("companyId")
    private Long getCompanyId() {
        return Optional
            .ofNullable(this.company)
            .map(Company::getId)
            .orElse(null);
    }

    @Builder
    private Computer(Long id, String name, Date introduced, Date discontinued, Company company) {
        this.id = id;
        this.name = name;
        this.introduced = introduced;
        this.discontinued = discontinued;
        this.company = company;
    }
}
