package cdb.repository.core;

import cdb.model.Company;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface CompanyRepository
{
    Optional<Company> findById(Long id);

    Page<Company> findAll(Pageable pageable);

    Company save(Company computer);

    void deleteById(Long id);
}
