package cdb.repository.core;

import cdb.model.Computer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface ComputerRepository
{
    Optional<Computer> findById(Long id);

    Page<Computer> findAll(Pageable pageable);

    Computer save(Computer computer);

    void deleteById(Long id);
}
