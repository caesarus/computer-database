package cdb.repository.jpa;

import cdb.model.Company;
import cdb.repository.core.CompanyRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JpaCompanyRepository extends CompanyRepository, JpaRepository<Company, Long> {}
