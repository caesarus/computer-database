package cdb.repository.jpa;

import cdb.model.Computer;
import cdb.repository.core.ComputerRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JpaComputerRepository extends ComputerRepository, JpaRepository<Computer, Long> {}
