package cdb.service.core;

import cdb.model.Company;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CompanyService
{
    Page<Company> pageAll(Pageable pageable);

    Company create(Company company);

    Company update(Long id, Company company);

    void delete(Long id);
}
