package cdb.service.core;

import cdb.model.Computer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ComputerService
{
    Page<Computer> pageAll(Pageable pageable);

    Computer create(Computer computer);

    Computer update(Long id, Computer computer);

    void delete(Long id);
}
