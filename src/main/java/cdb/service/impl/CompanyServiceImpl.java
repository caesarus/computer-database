package cdb.service.impl;

import cdb.model.Company;
import cdb.repository.core.CompanyRepository;
import cdb.service.core.CompanyService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.EntityNotFoundException;

@Service
public class CompanyServiceImpl implements CompanyService
{
    @Resource
    private CompanyRepository repository;

    @Override
    public Page<Company> pageAll(Pageable paging) {
        return repository.findAll(paging);
    }

    @Override
    public Company create(Company company) {
        company.setId(null); // safety: avoid to overwrite existing one
        return repository.save(company);
    }

    @Override
    public Company update(Long id, Company company) {
        Company changes = repository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Company with ID ["+ id +"] not found"));

        changes.apply(company);
        return repository.save(changes);
    }

    @Override
    public void delete(Long id) {
        repository.deleteById(id);
    }
}
