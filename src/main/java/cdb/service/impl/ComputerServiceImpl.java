package cdb.service.impl;

import cdb.model.Computer;
import cdb.repository.core.ComputerRepository;
import cdb.service.core.ComputerService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;

@Service
public class ComputerServiceImpl implements ComputerService
{
    @PersistenceContext
    private EntityManager entityManager;

    @Resource
    private ComputerRepository repository;

    @Override
    public Page<Computer> pageAll(Pageable paging) {
        return repository.findAll(paging);
    }

    @Override
    public Computer create(Computer computer) {
        computer.setId(null); // safety: avoid to overwrite existing one
        return repository.save(computer);
    }

    @Override
    public Computer update(Long id, Computer computer) {
        Computer changes = repository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Computer with ID ["+ id +"] not found"));

        entityManager.detach(changes);
        changes.apply(computer);

        return repository.save(changes);
    }

    @Override
    public void delete(Long id) {
        repository.deleteById(id);
    }
}
