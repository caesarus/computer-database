package cdb.valid.constraint;

import cdb.valid.validator.FutureDiscontinuedValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target(TYPE)
@Retention(RUNTIME)
@Constraint(validatedBy = {FutureDiscontinuedValidator.class})
public @interface FutureDiscontinued
{
    String message() default "discontinued value must be greater than introduced value";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
