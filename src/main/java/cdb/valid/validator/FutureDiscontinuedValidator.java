package cdb.valid.validator;

import cdb.model.Computer;
import cdb.valid.constraint.FutureDiscontinued;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Date;

@Component
public class FutureDiscontinuedValidator implements ConstraintValidator<FutureDiscontinued, Computer>
{
    @Override
    public void initialize(FutureDiscontinued constraintAnnotation) {}

    @Override
    public boolean isValid(Computer computer, ConstraintValidatorContext constraintValidatorContext) {
        if (computer == null) return true;

        Date introduced = computer.getIntroduced();
        Date discontinued = computer.getDiscontinued();

        if (introduced == null && discontinued == null) return true;
        if (introduced != null && discontinued == null) return true;

        return (introduced != null) && discontinued.after(introduced);
    }
}
