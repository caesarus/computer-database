package cdb.controller;

import cdb.BaseTest;
import io.restassured.RestAssured;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.Before;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.Resource;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
public abstract class BaseControllerTest extends BaseTest
{
    @Resource
    WebApplicationContext context;

    @LocalServerPort
    int port;

    @Before
    public void before() {
        RestAssuredMockMvc.webAppContextSetup(context);
        RestAssured.port = port;
    }
}
