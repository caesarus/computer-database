package cdb.controller;

import cdb.model.Company;
import cdb.service.core.CompanyService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.json.JsonContent;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import javax.annotation.Resource;
import javax.persistence.EntityNotFoundException;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class CompanyControllerTest extends BaseControllerTest
{
    @Resource
    private Pageable defaultPaging;

    @MockBean
    private CompanyService service;

    private Company company;

    @Before
    public void before() {
        super.before();
        company = Company.builder().id(206L).name("Fake name").build();
    }

    @Test
    public void should_page_all_with_paging() {
        Page<Company> page = new PageImpl<>(singletonList(company), PageRequest.of(1,6), 1);
        doReturn(page).when(service).pageAll(any());

        given()
                .contentType(JSON)
        .when()
                .get("/companies?page=1&size=6")
        .then()
                .statusCode(200)
                .body("totalElements", is(7))
                .body("pageable.pageSize", is(6))
                .body("content.id", hasItems(206));
    }

    @Test
    public void should_page_all_without_paging() {
        Page<Company> page = new PageImpl<>(singletonList(company), defaultPaging, 1);
        doReturn(page).when(service).pageAll(any());

        given()
                .contentType(JSON)
        .when()
                .get("/companies")
        .then()
                .statusCode(200)
                .body("totalElements", is(1))
                .body("pageable.pageSize", is(20))
                .body("content.id", hasItems(206));
    }

    @Test
    public void should_create_one() {
        doReturn(company).when(service).create(any());

        given()
                .contentType(JSON)
        .when()
                .body(company)
                .post("/companies")
        .then()
                .statusCode(201)
                .body("id", is(206))
                .body("name", is("Fake name"));
    }

    @Test
    public void should_update_one() {
        company.setName("Another fake");
        doReturn(company).when(service).update(any(), any());

        given()
                .contentType(JSON)
        .when()
                .body(company)
                .put("/companies/1")
        .then()
                .statusCode(200)
                .body("name", is("Another fake"));
    }

    @Test
    public void should_update_one_with_unknown_id() {
        doThrow(new EntityNotFoundException("unknown id 1000")).when(service).update(any(), any());

        given()
                .contentType(JSON)
        .when()
                .body(company)
                .put("/companies/1000")
        .then()
                .statusCode(404)
                .body(containsString("unknown id 1000"));
    }

    @Test
    public void should_delete_one() {
        doNothing().when(service).delete(any());

        given()
                .contentType(JSON)
        .when()
                .delete("/companies/1")
        .then()
                .statusCode(204);
    }
}