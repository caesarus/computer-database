package cdb.controller;

import cdb.model.Computer;
import cdb.service.core.ComputerService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import javax.annotation.Resource;
import javax.persistence.EntityNotFoundException;
import java.util.Date;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class ComputerControllerTest extends BaseControllerTest
{
    @Resource
    private Pageable defaultPaging;

    @MockBean
    private ComputerService service;

    private Computer computer;

    @Before
    public void before() {
        super.before();
        computer = Computer.builder().id(206L).name("Fake name").introduced(new Date(0)).build();
    }

    @Test
    public void should_page_all_with_paging() {
        Page<Computer> page = new PageImpl<>(singletonList(computer), PageRequest.of(1,6), 1);
        doReturn(page).when(service).pageAll(any());

        given()
                .contentType(JSON)
        .when()
                .get("/computers?page=1&size=6")
        .then()
                .statusCode(200)
                .body("totalElements", is(7))
                .body("pageable.pageSize", is(6))
                .body("content.id", hasItems(206));
    }

    @Test
    public void should_page_all_without_paging() {
        Page<Computer> page = new PageImpl<>(singletonList(computer), defaultPaging, 1);
        doReturn(page).when(service).pageAll(any());

        given()
                .contentType(JSON)
        .when()
                .get("/computers")
        .then()
                .statusCode(200)
                .body("totalElements", is(1))
                .body("pageable.pageSize", is(20))
                .body("content.id", hasItems(206));
    }

    @Test
    public void should_create_one() {
        doReturn(computer).when(service).create(any());

        given()
                .contentType(JSON)
        .when()
                .body(computer)
                .post("/computers")
        .then()
                .statusCode(201)
                .body("id", is(206))
                .body("name", is("Fake name"))
                .body("introduced", is(0));
    }

    @Test
    public void should_update_one() {
        computer.setName("Another fake");
        doReturn(computer).when(service).update(any(), any());

        given()
                .contentType(JSON)
        .when()
                .body(computer)
                .put("/computers/1")
        .then()
                .statusCode(200)
                .body("name", is("Another fake"));
    }

    @Test
    public void should_update_one_with_unknown_id() {
        doThrow(new EntityNotFoundException("unknown id 1000")).when(service).update(any(), any());

        given()
                .contentType(JSON)
        .when()
                .body(computer)
                .put("/computers/1000")
        .then()
                .statusCode(404)
                .body(containsString("unknown id 1000"));
    }

    @Test
    public void should_delete_one() {
        doNothing().when(service).delete(any());

        given()
                .contentType(JSON)
        .when()
                .delete("/computers/1")
        .then()
                .statusCode(204);
    }
}