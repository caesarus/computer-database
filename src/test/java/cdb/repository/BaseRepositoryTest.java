package cdb.repository;

import cdb.BaseTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;

import static org.springframework.test.annotation.DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD;

@Sql({ "classpath:/data/1-schema.sql", "classpath:/data/2-entries.sql" })
@DirtiesContext(classMode = BEFORE_EACH_TEST_METHOD )
public abstract class BaseRepositoryTest extends BaseTest {}
