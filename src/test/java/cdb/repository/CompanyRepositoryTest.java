package cdb.repository;

import cdb.model.Company;
import cdb.repository.core.CompanyRepository;
import org.junit.Test;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertTrue;

public class CompanyRepositoryTest extends BaseRepositoryTest
{
    @Resource
    private CompanyRepository repository;

    @Test
    public void should_find_by_id() {
        Optional<Company> maybeCompany = repository.findById(1L);

        assertTrue(maybeCompany.isPresent());
        assertThat(maybeCompany.get().getName(), is("Apple Inc."));
    }

    @Test
    public void should_find_all() {
        Page<Company> results = repository.findAll(PageRequest.of(0, 6));

        assertThat(results.getTotalElements(), is(42L));
        assertThat(results.getTotalPages(), is(7));

        List<Company> companies = results.getContent();
        assertThat(companies.size(), is(6));

        Company company = companies.get(0);
        assertThat(company.getName(), is("Apple Inc."));
    }

    @Test
    public void should_save_new_company() {
        Company company = Company.builder()
                .name("Takima")
                .build();

        Company created = repository.save(company);

        assertThat(created.getId(), is(44L));
    }

    @Test
    public void should_save_company_updates() {
        Company apple = Company.builder()
                .id(1L)
                .name("Takima")
                .build();

        Company updated = repository.save(apple);

        assertThat(updated.getId(), is(1L));
        assertThat(updated.getName(), is("Takima"));
    }

    @Test
    public void should_delete() {
        repository.deleteById(1L);

        Page<Company> lefts = repository.findAll(PageRequest.of(0,1));
        assertThat(lefts.getTotalElements(), is(41L));
    }
}