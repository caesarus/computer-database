package cdb.repository;

import cdb.model.Computer;
import cdb.repository.core.ComputerRepository;
import org.junit.Test;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertTrue;

public class ComputerRepositoryTest extends BaseRepositoryTest
{
    @Resource
    private ComputerRepository repository;

    @Test
    public void should_find_by_id() {
        Optional<Computer> maybeComputer = repository.findById(1L);

        assertTrue(maybeComputer.isPresent());
        assertThat(maybeComputer.get().getName(), is("MacBook Pro 15.4 inch"));
    }

    @Test
    public void should_find_all() {
        Page<Computer> results = repository.findAll(PageRequest.of(0, 6));

        assertThat(results.getTotalElements(), is(574L));
        assertThat(results.getTotalPages(), is(96));

        List<Computer> computers = results.getContent();
        assertThat(computers.size(), is(6));

        Computer computer = computers.get(0);
        assertThat(computer.getName(), is("MacBook Pro 15.4 inch"));
    }

    @Test
    public void should_save_new_computer() {
        Computer computer = Computer.builder()
                .name("Takima computer")
                .build();

        Computer created = repository.save(computer);

        assertThat(created.getId(), is(575L));
    }

    @Test
    public void should_save_computer_updates() {
        Computer apple = Computer.builder()
            .id(1L)
            .name("Takima computer")
            .company(null)
            .build();

        Computer updated = repository.save(apple);

        assertThat(updated.getId(), is(1L));
        assertThat(updated.getName(), is("Takima computer"));
        assertThat(updated.getCompany(), nullValue());
    }

    @Test
    public void should_delete() {
        repository.deleteById(1L);

        Page<Computer> lefts = repository.findAll(PageRequest.of(0,1));
        assertThat(lefts.getTotalElements(), is(573L));
    }
}