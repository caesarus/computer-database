package cdb.service;

import cdb.BaseTest;
import cdb.model.Company;
import cdb.repository.core.CompanyRepository;
import cdb.service.core.CompanyService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import javax.annotation.Resource;
import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class CompanyServiceTest extends BaseTest
{
    @MockBean
    private CompanyRepository repository;

    @Resource
    private CompanyService service;

    private Company company;
    private List<Company> companies;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);

        company = Company.builder().id(1L).name("Apple Inc.").build();
        companies = new ArrayList<>(asList(
                company,
                Company.builder().id(2L).name("Thinking Machines").build(),
                Company.builder().id(3L).name("RCA").build()
        ));
    }

    @Test
    public void should_page_all() {
        Page<Company> paging = new PageImpl<>(companies, PageRequest.of(2,4), 12);
        doReturn(paging).when(repository).findAll(any());

        Page<Company> paged = service.pageAll(PageRequest.of(2,4));

        assertThat(paged.getTotalElements(), is(12L));
        assertThat(paged.getTotalPages(), is(3));

        Company first = paged.getContent().get(0);
        assertThat(first.getName(), is("Apple Inc."));
    }

    @Test
    public void should_create() {
        doReturn(company).when(repository).save(any());

        Company created = service.create(Company.builder().build());

        assertThat(created.getId(), notNullValue());
    }

    @Test
    public void should_update() {
        doReturn(Optional.of(company)).when(repository).findById(any());
        doReturn(company).when(repository).save(any());

        Company created = service.update(0L, Company
                .builder().id(0L).name("Takima").build()
        );

        assertThat(created.getId(), is(0L));
        assertThat(created.getName(), is("Takima"));
    }

    @Test(expected = EntityNotFoundException.class)
    public void fail_update_with_unknown_id() {
        doThrow(new EntityNotFoundException()).when(repository).findById(any());

        service.update(1000L, Company.builder().build());
    }

    @Test
    public void should_delete() {
        doNothing().when(repository).deleteById(any());

        service.delete(1L);
    }
}
