package cdb.service;

import cdb.BaseTest;
import cdb.model.Company;
import cdb.model.Computer;
import cdb.repository.core.ComputerRepository;
import cdb.service.core.ComputerService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import javax.annotation.Resource;
import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class ComputerServiceTest extends BaseTest
{
    @MockBean
    private ComputerRepository repository;

    @Resource
    private ComputerService service;

    private Computer computer;
    private List<Computer> computers;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);

        computer = Computer.builder().id(1L).name("MacBook Pro 15.4 inch").build();
        computers = new ArrayList<>(asList(
                computer,
                Computer.builder().id(2L).name("CM-2a").build(),
                Computer.builder().id(3L).name("CM-200").build()
        ));
    }

    @Test
    public void should_page_all() {
        Page<Computer> paging = new PageImpl<>(computers, PageRequest.of(2,4), 12);
        doReturn(paging).when(repository).findAll(any());

        Page<Computer> paged = service.pageAll(PageRequest.of(2,4));

        assertThat(paged.getTotalElements(), is(12L));
        assertThat(paged.getTotalPages(), is(3));

        Computer first = paged.getContent().get(0);
        assertThat(first.getName(), is("MacBook Pro 15.4 inch"));
    }

    @Test
    public void should_create() {
        doReturn(computer).when(repository).save(any());

        Computer created = service.create(Computer.builder().build());

        assertThat(created.getId(), notNullValue());
    }

    @Test
    public void should_update() {
        Company company = Company.builder().id(1L).name("Takima").build();

        doReturn(Optional.of(computer)).when(repository).findById(any());
        doReturn(computer).when(repository).save(any());

        Computer created = service.update(0L, Computer
                .builder().id(0L).name("Takima computer")
                .company(company).build()
        );

        assertThat(created.getId(), is(1L));
        assertThat(created.getName(), is("Takima computer"));
        assertThat(created.getCompany(), notNullValue());
        assertThat(created.getCompany().getId(), is(1L));
        assertThat(created.getCompany().getName(), is("Takima"));
    }

    @Test(expected = EntityNotFoundException.class)
    public void fail_update_when_unknown_id() {
        doThrow(new EntityNotFoundException()).when(repository).findById(any());

        service.update(1000L, Computer.builder().build());
    }

    @Test
    public void should_delete() {
        doNothing().when(repository).deleteById(any());

        service.delete(1L);
    }
}
